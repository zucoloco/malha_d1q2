#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

A slab initially at temperature equal to zero, T = 0.0. For time >= 0, the left 
surface of the slab is subjected to a high temperature and equal to unity, 
T = 1.0. The slab length is 100 units. Calculate the temperature distribution
in the slab for t = 200. alpha = 0.25
"""

import numpy as np
import matplotlib.pylab as plt 

T = np.zeros(100) 
feq = np.zeros(100)
omega = 4.0/3.0 
f = np.zeros([100, 2])
f_linha = np.zeros([100, 2]) #para atualizar o vetor na propagação sem sobrepor valores

for t in range(200):
    for x in range(0,100):
        T[x] = sum(f[x, :])
        feq[x] = T[x] * 0.5
        
        for k in range(2):
            f[x, k] = f[x, k]*(1 - omega) + omega * feq[x] #colisão
        
    for x in range(1, 99): #propagação
        f_linha[x, 0] = f[x+1, 0]
        f_linha[x, 1] = f[x-1, 1]
        
    f_linha[0, 0] = f[1, 0];
    f_linha[99, 1] = f[98, 1]
    
    f_linha[0, 1] = 1.0 - f_linha[0, 0]
    
    tmp = f
    f = f_linha
    f_linha = tmp 
    
plt.plot(T)
plt.show()
    
 
